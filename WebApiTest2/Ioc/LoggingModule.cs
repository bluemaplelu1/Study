﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac.Core;
using log4net;
using Module = Autofac.Module;
using System.Reflection;
namespace WebApiTest2.Ioc
{
    public class LoggingModule:Module
    {
        private static void InjectLoggerProperties(object instance)
        {
            var instanceType = instance.GetType();
            //get all injectable properties to set;
            var properties = instanceType
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.PropertyType == typeof(ILog) && x.CanWrite && x.GetIndexParameters().Length == 0);
            //Set properties located
            foreach (var proptoset in properties)
            {
                proptoset.SetValue(instance, LogManager.GetLogger(instanceType), null);
            }
        }

        private static void OnComponentPreparing(object sender, PreparingEventArgs e)
        {
            e.Parameters = e.Parameters.Union(
                new[] 
                {
                    new ResolvedParameter(
                        (p,i)=>p.ParameterType==typeof(ILog),
                        (p,i)=>LogManager.GetLogger(p.Member.DeclaringType)
                        ),
                });
        }
        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            //base.AttachToComponentRegistration(componentRegistry, registration);
            registration.Preparing += OnComponentPreparing;
            registration.Activated += (sender, e) => InjectLoggerProperties(e.Instance);
        }
    }
}