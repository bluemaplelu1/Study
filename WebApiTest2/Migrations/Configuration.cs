namespace WebApiTest2.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Configuration;
    internal sealed class Configuration : DbMigrationsConfiguration<WebApiTest2.Entity.DB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true; //自动更新数据库
            AutomaticMigrationDataLossAllowed = true; //重命名和删除表字段时会丢失数据，设置成允许，否则次情况下同步数据库会出错
            ContextKey = "WebApiTest2.Entity.DB";//
            //var providerName = ConfigurationManager.ConnectionStrings["DBConnection"].ProviderName;
            //if (providerName == "MySql.Data.MySqlClient")
            //{
            //    SetSqlGenerator("MySql.Data.MySqlClient",new MySql.Data.Entity.MySqlMigrationSqlGenerator());
            //}
        }

        protected override void Seed(WebApiTest2.Entity.DB context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
