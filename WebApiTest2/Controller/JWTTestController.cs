﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiTest2.Common;
using System.Security.Claims;

namespace WebApiTest2.Controller
{
    [RoutePrefix("api/jwt")]
    public class JWTTestController : ApiController
    {
        [Route("token"),HttpGet]
        public IHttpActionResult GetToken()
        {
            var dic = new Dictionary<string, object>();
            foreach (var item in Request.GetQueryNameValuePairs())
            {
                dic.Add(item.Key, item.Value);                
            }
            var token = new JWTHelper().Encode(dic, "JWTTest", 30);
            return Ok(token);
        }
        [Route("GetUserInfoFromToken"),HttpGet]
        public IHttpActionResult GetUser()
        {
            var user=(ClaimsPrincipal)User;
            var dic = new Dictionary<string, object>();
            foreach (var userClaim in user.Claims)
            {
                dic.Add(userClaim.Type, userClaim.Value);
            }
            return Ok(dic);
        }
    }
}
