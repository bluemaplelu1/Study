﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using WebApiTest2.Config;
using Autofac.Integration.WebApi;

//标识webapiOwin.Startup类为owin的启动类，也可以写在AssmblyInfo.cs文件
[assembly: OwinStartup(typeof(WebApiTest2.Startup))]

namespace WebApiTest2
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // 有关如何配置应用程序的详细信息，请访问 https://go.microsoft.com/fwlink/?LinkID=316888            

            //获取webapi配置
            var config = WebApiConfig.OwinWebApiConfiguration(new HttpConfiguration());
            //获取webapi的依赖注入容器
            var containter = ContainerBuilderCommon.GetWebApiContainer();
            //配置webapi的依赖注入
            config.DependencyResolver = new AutofacWebApiDependencyResolver(containter);

            #region Owin组件注册         
            app.UseAutofacMiddleware(containter);            
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
            //app.Run(ctx => 
            //{
            //    ctx.Response.Write("this is a owin pipline");
            //    return Task.FromResult(0);
            //});
            #endregion
        }
    }
}
