﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiTest2.Exception;

namespace WebApiTest2.Controller
{
    [RoutePrefix("api/exceptionTest")]
    public class ExceptionTestController : ApiController
    {
        [Route("know"),HttpGet]
        public IHttpActionResult Konw()
        {
            throw new System.Exception("已知异常");
        }
        [Route("unknow"),HttpGet]
        public IHttpActionResult Unkonw()
        {
            throw new System.Exception("未知异常");
        }
    }
}
