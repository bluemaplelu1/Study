﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using WebApiTest2.Config;
using JWT;
using WebApiTest2.Common;
namespace WebApiTest2.Security
{
    public class IdentityBasicAuthentication : IAuthenticationFilter
    {
        //public bool IFilter.AllowMultiple => throw new NotImplementedException();
        public bool AllowMultiple { get; }        

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            //1.获取token
            context.Request.Headers.TryGetValues("token", out var tokenHeader);
            //2.如果没有token，不做任何处理
            if (tokenHeader == null || !tokenHeader.Any())
            {
                return Task.FromResult(0);
            }
            //3.如果token验证通过，则写入到Identity,如果未通过则设置错误
            var jwtHelper = new JWTHelper();
            var payloadClaims = jwtHelper.DecodeToObject(tokenHeader.FirstOrDefault(), Config.Config.JWTKey, out bool isValid, out string errMsg);
            if (isValid)
            {
                //只要ClaimsIdentity设置了authenticationType,authenticated 就为true，后面的authority根据authenticated=true来做权限
                var identity = new ClaimsIdentity("jwt", "userId", "roles");
                foreach (var keyValuePair in  payloadClaims)
                {
                    identity.AddClaim(new Claim(keyValuePair.Key,keyValuePair.Value.ToString()));
                }
                //最好是http上下文的principal和进程的currentPrincipal都设置
                context.Principal = new ClaimsPrincipal(identity);
                Thread.CurrentPrincipal = new ClaimsPrincipal(identity);
            }
            else
            {
                context.ErrorResult = new ResponseMessageResult(new HttpResponseMessage()
                {
                    StatusCode = System.Net.HttpStatusCode.ProxyAuthenticationRequired,
                    Content = new StringContent(errMsg)
                });
            }
            //throw new NotImplementedException();
            return Task.FromResult(0);
        }
        

        Task IAuthenticationFilter.ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }
    }
}