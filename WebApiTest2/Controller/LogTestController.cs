﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;

namespace WebApiTest2.Controller
{
    public class LogTestController : ApiController
    {
        private ILog _log;
        public LogTestController(ILog log)
        {
            this._log = log;
        }
        public IHttpActionResult Get()
        {
            _log.Debug("测试debug", new System.Exception("debug异常"));
            _log.Info("测试Info", new System.Exception("Info异常"));
            _log.Warn("测试Warn", new System.Exception("Warn异常"));
            _log.Error("测试Error", new System.Exception("Error异常"));
            _log.Fatal("测试Fatal", new System.Exception("Fatal异常"));
            return Ok("日志已写入");

        }
    }
}
