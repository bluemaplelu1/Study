﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebApiTest2.Entity
{
    public class DB:DbContext
    {
        /// <summary>
        /// name=DBConnection，DBConnection为数据库连接的名字，即web.config配置文件节点connectionStrings，name值为DBConnection的数据库连接字符串
        /// </summary>
        public DB():base("name=DBConnection")
        {
            //默认策略为CreateDatabaseIfNotExists,即如果不存在则创建，用migration时修改成MigrationDatabaseToLatestVersion即每首次访问db时同步最新数据库结构
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DB,WebApiTest2.Migrations.Configuration>("DBConnection"));
        }

        //配置所有的数据库表
        public DbSet<TestTable> TestTables { get; set; }
    }
}