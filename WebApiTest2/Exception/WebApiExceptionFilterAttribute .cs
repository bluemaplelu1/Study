﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using log4net;
using System.Web.Http.Filters;
using System.Net;

namespace WebApiTest2.Exception
{
    public class WebApiExceptionFilterAttribute: ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var exception = actionExecutedContext.Exception;//获取产生的异常
            var exceptionMessage = exception.Message;
            var logMessage = $@"contoller.action={actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName}.
{actionExecutedContext.ActionContext.ActionDescriptor.ActionName}:exception={exception.Message}";
            ILog log = LogManager.GetLogger(actionExecutedContext.ActionContext.ControllerContext.Controller.GetType());
            if (exception is KnowException)
            {
                log.Debug(logMessage);
            }
            else
            {
                log.Error(logMessage, exception);
            }
            actionExecutedContext.Response=actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, exceptionMessage);
            //base.OnException(actionExecutedContext);
        }
    }
}