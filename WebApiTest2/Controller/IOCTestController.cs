﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiTest2.Ioc;

namespace WebApiTest2.Controller
{
    public class IOCTestController : ApiController
    {
        private People _people;

        public IOCTestController(People people)
        {
            _people = people;
        }

        public IHttpActionResult GetLanguage()
        {
            return Ok(_people.Language());
        }
    }
}
