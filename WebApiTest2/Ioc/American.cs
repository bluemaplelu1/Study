﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTest2.Ioc
{
    public class American : People
    {
        public string Language()
        {
            return "English";
        }
    }
}