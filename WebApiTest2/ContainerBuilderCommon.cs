﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using WebApiTest2.Ioc;

namespace WebApiTest2
{
    public static class ContainerBuilderCommon
    {
        public static IContainer GetWebApiContainer()
        {
            var builder = new ContainerBuilder(); //创建容器
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()); //注册webapi的所有控制器
            builder.RegisterModule<LoggingModule>(); //注册moudle
            
            /*测试模块*/ //注册测试模块

            builder.RegisterType<Chinese>().As<People>();//注册类型
            return builder.Build();
        }
    }
}