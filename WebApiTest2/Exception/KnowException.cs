﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WebApiTest2.Exception
{
    public class KnowException: System.Exception
    {
        public KnowException() : base()
        {
        }

        public KnowException(string message) : base(message)
        {
        }

        public KnowException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

        protected KnowException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}