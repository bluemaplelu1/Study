﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiTest2.Entity
{
    [Table("Test")]
    public class TestTable
    {
        [Key,Column(TypeName ="nvarchar"),MaxLength(50)]
        public string Id { get; set; }
        [Column(TypeName ="int")]
        public int? Age { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreateTime { get; set; }
        [Column(TypeName ="nvarchar"),MaxLength(50)]
        public string Name { get; set; }
    }
}