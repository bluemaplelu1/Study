﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApiTest2.Exception;
using WebApiTest2.Security;

namespace WebApiTest2.Config
{
    public static class WebApiConfig
    {
        public static HttpConfiguration OwinWebApiConfiguration(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();//启用特性路由
            config.Routes.MapHttpRoute(
                name:"DefaultApi",
                routeTemplate:"api/{Controller}/{id}",
                defaults:new { id=RouteParameter.Optional}                
                );
            config.Filters.Add(new WebApiExceptionFilterAttribute());
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Filters.Add(new IdentityBasicAuthentication());
            return config;
        }
    }
}