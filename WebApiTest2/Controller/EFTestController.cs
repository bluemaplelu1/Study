﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiTest2.Entity;

namespace WebApiTest2.Controller
{
    public class EFTestController : ApiController
    {
        public IHttpActionResult Get()
        {
            using (DB db= new DB())
            {
                var list = db.TestTables;
                return Ok(list.ToList());
            }
        }
    }
}
